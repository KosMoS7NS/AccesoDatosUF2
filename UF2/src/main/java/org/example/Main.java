package org.example;

import org.example.database.Connect;
import org.example.database.create.CreateTable;
import org.example.database.create.InsertRow;
import org.example.database.delete.DeleteRow;
import org.example.database.delete.DeleteTable;
import org.example.database.read.ReadRow;
import org.example.database.read.ReadTable;
import org.example.database.update.UpdateField;
import org.example.database.update.UpdateRow;
import org.example.shared.Menu;

import java.sql.SQLException;

import static org.example.shared.Menu.*;

/**
 * Dependiendo de la opción del usuario que elija en el menú, seleccionará un método u otro definido en otras clases.
 *
 * @author Daniel Díez Miguel
 */
public class Main {
    /**
     *
     * @param args
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        new Connect();

        int i = 1;
        while (i != 0) {
            switch (new Menu().menuFeature()) {
                case 1 -> new CreateTable().crearTabla(TABLA, ID_FIELD, NOMBRE_FIELD, ALTURA_FIELD,
                        PRIMERA_ASCENSION_FIELD, REGION_FIELD, PAIS_FIELD);
                case 2 -> new InsertRow().insertRow(TABLA, NOMBRE_FIELD, NOMBRE_NEW_VALOR,
                        ALTURA_FIELD, ALTURA_NEW_VALOR, PRIMERA_ASCENSION_FIELD, PRIMERA_ASCENSION_NEW_VALOR,
                        REGION_FIELD, REGION_NEW_VALOR, PAIS_FIELD, PAIS_NEW_VALOR);
                case 3 -> new ReadTable().readTable(TABLA);
                case 4 -> new ReadRow().readRow(TABLA, FIELD, VALUE);
                case 6 -> new DeleteRow().deleteRow(TABLA, FIELD, OLD_VALUE);
                case 7 -> new DeleteTable().deleteTable(TABLA);
                case 8 -> new UpdateField().updateField(TABLA, FIELD, NEW_VALUE, OLD_VALUE);
                case 9 -> new UpdateRow().updateRow(TABLA, NOMBRE_FIELD, NOMBRE_NEW_VALOR, NOMBRE_OLD_VALOR,
                        ALTURA_FIELD, ALTURA_NEW_VALOR, PRIMERA_ASCENSION_FIELD, PRIMERA_ASCENSION_NEW_VALOR,
                        REGION_FIELD, REGION_NEW_VALOR, PAIS_FIELD, PAIS_NEW_VALOR, FIELD, OLD_VALUE);
                default -> i = 0;
            }
        }
        new Connect().disconnect();
    }
}