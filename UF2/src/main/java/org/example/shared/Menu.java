package org.example.shared;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.in;
import static java.lang.System.out;

/**
 * Esta clase cuenta con el método de menú.
 */
public class Menu {
    public static String ID_FIELD;
    public static String VALUE;
    public static String NEW_VALUE;
    public static String FIELD;
    public static String OLD_VALUE;
    public static String TABLA;
    public static String NOMBRE_FIELD;
    public static String ALTURA_FIELD;
    public static String PRIMERA_ASCENSION_FIELD;
    public static String REGION_FIELD;
    public static String PAIS_FIELD;
    public static String NOMBRE_NEW_VALOR;
    public static int ALTURA_NEW_VALOR;
    public static int PRIMERA_ASCENSION_NEW_VALOR;
    public static String REGION_NEW_VALOR;
    public static String PAIS_NEW_VALOR;
    public static String NOMBRE_OLD_VALOR;
    private final Scanner scanner = new Scanner(in);
    private int respuesta;

    /**
     * El método cuenta con un menú principal y otro secundario que se mostrará dependiendo de la respuesta del usuario.
     * Se devuelve la respuesta del usuario al Main, según esta respuesta llamará al método correspondiente para ejecutar
     * la funcionalidad.
     *
     * @return respuesta
     */
    public int menuFeature() {
        List menu_principal = Arrays.asList(
                "|================================|",
                "|1) Crear tabla                  |",
                "|2) Añadir fila                  |",
                "|3) Leer tabla                   |",
                "|4) Leer fila                    |",
                "|5) Modificar fila               |",
                "|6) Eliminar fila                |",
                "|7) Eliminar tabla               |",
                "|0) Salir                        |",
                "|================================|");

        List menu_secundario = Arrays.asList(
                "|================================|",
                "|8) Modificar 1 campo            |",
                "|9) Modificar varios campos      |",
                "|10) Salir                       |",
                "|================================|");


        menu_principal.forEach(out::println);
        out.println("Introduce una respuesta: ");
        respuesta = scanner.nextInt();

        if (respuesta < 1 || respuesta > 9) respuesta = 0;

        if (respuesta != 0) {
            out.println("Introduce el nombre de la tabla: ");
            TABLA = scanner.next();
        }

        switch (respuesta) {
            case 1:
                out.println("Introduce campo id: ");
                ID_FIELD = scanner.next();

                out.println("Introduce campo nombre: ");
                NOMBRE_FIELD = scanner.next();

                out.println("Introduce campo altura: ");
                ALTURA_FIELD = scanner.next();

                out.println("Introduce campo primera ascensión: ");
                PRIMERA_ASCENSION_FIELD = scanner.next();

                out.println("Introduce campo región: ");
                REGION_FIELD = scanner.next();

                out.println("Introduce campo país: ");
                PAIS_FIELD = scanner.next();
                break;

            case 2:
                out.println("Introduce campo nombre: ");
                NOMBRE_FIELD = scanner.next();

                out.println("Introduce el nuevo valor de nombre: ");
                NOMBRE_NEW_VALOR = scanner.next();

                out.println("Introduce campo altura: ");
                ALTURA_FIELD = scanner.next();

                out.println("Introduce el nuevo valor de altura: ");
                ALTURA_NEW_VALOR = scanner.nextInt();

                out.println("Introduce campo primera ascensión: ");
                PRIMERA_ASCENSION_FIELD = scanner.next();

                out.println("Introduce el nuevo valor de primera ascension: ");
                PRIMERA_ASCENSION_NEW_VALOR = scanner.nextInt();

                out.println("Introduce campo región: ");
                REGION_FIELD = scanner.next();

                out.println("Introduce el nuevo valor de región: ");
                REGION_NEW_VALOR = scanner.next();

                out.println("Introduce campo país: ");
                PAIS_FIELD = scanner.next();

                out.println("Introduce el nuevo valor de país: ");
                PAIS_NEW_VALOR = scanner.next();
                break;

            case 4:
                out.println("Introduce nombre del campo a buscar: ");
                FIELD = scanner.next();

                out.println("Introduce nombre del valor: ");
                VALUE = scanner.next();
                break;

            case 5:
                menu_secundario.forEach(out::println);
                out.println("Introduce una respuesta: ");
                respuesta = scanner.nextInt();

                if (respuesta == 8) {
                    out.println("Introduce el nombre del campo a editar: ");
                    FIELD = scanner.next();

                    out.println("Introduce el viejo valor: ");
                    OLD_VALUE = scanner.next();

                    out.println("Introduce el nuevo valor: ");
                    NEW_VALUE = scanner.next();

                } else if (respuesta == 9) {
                    out.println("Introduce campo nombre: ");
                    NOMBRE_FIELD = scanner.next();

                    out.println("Introduce el nuevo valor de nombre: ");
                    NOMBRE_NEW_VALOR = scanner.next();

                    out.println("Introduce campo altura: ");
                    ALTURA_FIELD = scanner.next();

                    out.println("Introduce el nuevo valor de altura: ");
                    ALTURA_NEW_VALOR = scanner.nextInt();

                    out.println("Introduce campo primera ascensión: ");
                    PRIMERA_ASCENSION_FIELD = scanner.next();

                    out.println("Introduce el nuevo valor de primera ascension: ");
                    PRIMERA_ASCENSION_NEW_VALOR = scanner.nextInt();

                    out.println("Introduce campo región: ");
                    REGION_FIELD = scanner.next();

                    out.println("Introduce el nuevo valor de región: ");
                    REGION_NEW_VALOR = scanner.next();

                    out.println("Introduce campo país: ");
                    PAIS_FIELD = scanner.next();

                    out.println("Introduce el nuevo valor de país: ");
                    PAIS_NEW_VALOR = scanner.next();

                    out.println("Introduce el campo a buscar: ");
                    FIELD = scanner.next();

                    out.println("Introduce el antiguo valor: ");
                    OLD_VALUE = scanner.next();
                }
                break;

            case 6:
                out.println("Introduce el nombre del campo a borrar: ");
                FIELD = scanner.next();

                out.println("Introduce el valor actual: ");
                OLD_VALUE = scanner.next();
                break;
        }
        return respuesta;
    }
}
