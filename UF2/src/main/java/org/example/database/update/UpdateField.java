package org.example.database.update;

import java.sql.SQLException;
import java.sql.Statement;

import static org.example.database.Connect.CONNECTION;

/**
 * Método en el cual se realiza un update de la tabla seleccionada por el usuario.
 * Puede elegir además, los campos y valores.
 * Solo se actualiza un campo.
 */
public class UpdateField {
    /**
     *
     * @param table
     * @param field
     * @param newValue
     * @param oldValue
     * @throws SQLException
     */
    public void updateField(String table, String field, String newValue, String oldValue) throws SQLException {
        Statement st = CONNECTION.createStatement();
        st.executeUpdate(
                "UPDATE " + table +
                        " SET " + field + " = '" + newValue + "' " +
                        " WHERE " + field + " = '" + oldValue + "' ");
        if (st != null) st.close();
    }
}
