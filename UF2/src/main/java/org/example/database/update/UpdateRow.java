package org.example.database.update;

import java.sql.SQLException;
import java.sql.Statement;

import static org.example.database.Connect.CONNECTION;

/**
 * Método en el cual se realiza un update de la tabla seleccionada por el usuario.
 * Puede elegir además, todos los campos y valores.
 * Se actualiza todos los registros de la tabla.
 */
public class UpdateRow {
    /**
     *
     * @param table
     * @param nombreField
     * @param nombreNewValor
     * @param nombreOldValor
     * @param alturaField
     * @param alturaNewValor
     * @param primeraAscensionField
     * @param primeraAscensionNewValor
     * @param regionField
     * @param regionNewValor
     * @param paisField
     * @param paisNewValor
     * @param field
     * @param oldValor
     * @throws SQLException
     */
    public void updateRow(String table, String nombreField, String nombreNewValor, String nombreOldValor,
                          String alturaField, int alturaNewValor, String primeraAscensionField, int primeraAscensionNewValor,
                          String regionField, String regionNewValor, String paisField, String paisNewValor,
                          String field, String oldValor) throws SQLException {
        Statement st = CONNECTION.createStatement();
        st.executeUpdate(
                "UPDATE " + table +
                        " SET " + nombreField + " = '" + nombreNewValor + "', " +
                        "" + alturaField + " = '" + alturaNewValor + "', " +
                        "" + primeraAscensionField + " = '" + primeraAscensionNewValor + "', " +
                        "" + regionField + " = '" + regionNewValor + "', " +
                        "" + paisField + " = '" + paisNewValor + "' " +
                        " WHERE " + field + " = '" + oldValor + "' ");
        if (st != null) st.close();
    }
}
