package org.example.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static java.lang.System.out;

/**
 * Se establece el usuario, password, url y driver de la bbdd.
 * Dispone de un método de conexión a la bbdd y de desconexión.
 */
public class Connect {
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String user = "root";
    private static final String password = "";
    private static final String url = "jdbc:mysql://localhost:3306/uf2";
    public static Connection CONNECTION;

    /**
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public Connect() throws SQLException, ClassNotFoundException {
        Class.forName(driver);
        CONNECTION = DriverManager.getConnection(url, user, password);
    }

    public void disconnect() {
        CONNECTION = null;
    }

}
