package org.example.database.create;

import java.sql.SQLException;
import java.sql.Statement;

import static org.example.database.Connect.CONNECTION;

/**
 * Se inserta registros en la tabla en la cual se pasa como variable la que desee el usuario.
 * Los campos y registros los decide el usuario para tener su tabla personalizada.
 */
public class InsertRow {
    /**
     *
     * @param table
     * @param nombreField
     * @param nombreValor
     * @param alturaField
     * @param alturaValor
     * @param primeraAscensionField
     * @param primeraAscensionValor
     * @param regionField
     * @param regionValor
     * @param paisField
     * @param paisValor
     * @throws SQLException
     */
    public void insertRow(String table, String nombreField, String nombreValor, String alturaField, int alturaValor,
                          String primeraAscensionField, int primeraAscensionValor, String regionField,
                          String regionValor, String paisField, String paisValor)
            throws SQLException {
        Statement st = CONNECTION.createStatement();
        st.execute("INSERT INTO " + table + "(" + nombreField + ", " + alturaField + ", " + primeraAscensionField + ", "
                + regionField + ", " + paisField + ")" +
                "VALUES('" + nombreValor + "', " + alturaValor + ", " + primeraAscensionValor + ", '" +
                regionValor + "', '" + paisValor + "')");
        if (st != null) st.close();
    }
}
