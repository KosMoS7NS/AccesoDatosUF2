package org.example.database.create;

import java.sql.SQLException;
import java.sql.Statement;

import static org.example.database.Connect.CONNECTION;

/**
 * Se crea la tabla con los campos de id, nombre, altura, primeraAscension, region y pais.
 * Se le pasa como parámetro el nombre de la tabla, id, nombre, altura, primeraAscension, región y país.
 */
public class CreateTable {
    private static Statement st;
    /**
     *
     * @param tabla
     * @param id
     * @param nombre
     * @param altura
     * @param primeraAscension
     * @param region
     * @param pais
     * @throws SQLException
     */
    public void crearTabla(String tabla, String id, String nombre, String altura,
                           String primeraAscension, String region, String pais) throws SQLException {
        st = CONNECTION.createStatement();
        st.executeUpdate("CREATE TABLE " + tabla + "("
                + id + " SERIAL PRIMARY KEY, "
                + nombre + " TEXT, "
                + altura + " INT(40), "
                + primeraAscension + " INT(40), "
                + region + " VARCHAR(40), "
                + pais + " VARCHAR(40)"
                + ")");
        if (st != null) st.close();

    }
}

