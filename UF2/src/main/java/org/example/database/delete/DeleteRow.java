package org.example.database.delete;

import java.sql.SQLException;
import java.sql.Statement;

import static org.example.database.Connect.CONNECTION;

/**
 * Método en el cual se borra la fila dado por el usuario.
 * Puede seleccionar a partir de que campo con su registro puede apuntar para el borrado.
 */
public class DeleteRow {
    /**
     *
     * @param table
     * @param field
     * @param value
     * @throws SQLException
     */
    public void deleteRow(String table, String field, String value) throws SQLException {
        Statement st = CONNECTION.createStatement();
        st.executeUpdate(
                "DELETE FROM " + table + " WHERE " + field + " = '" + value + "' ");
        if (st != null) st.close();
    }
}
