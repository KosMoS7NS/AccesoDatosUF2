package org.example.database.delete;

import java.sql.SQLException;
import java.sql.Statement;

import static org.example.database.Connect.CONNECTION;

/**
 * Se borra la tabla a partir del nombre dado por el usuario
 */
public class DeleteTable {
    /**
     *
     * @param table
     * @throws SQLException
     */
    public void deleteTable(String table) throws SQLException {
        Statement st = CONNECTION.createStatement();
        st.executeUpdate("DROP TABLE " + table + "");
        if (st != null) st.close();
    }
}
