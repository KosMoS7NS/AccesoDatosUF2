package org.example.database.read;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;
import static org.example.database.Connect.CONNECTION;

/**
 * Método que realiza una SQL en el cual muestra todos los datos de la tabla.
 * El usuario puede seleccionar la tabla que desee.
 * Mientras haya un ResultSet, va a ir almacenando en las variables respectivas para luego ser mostradas.
 */
public class ReadTable {
    /**
     *
     * @param table
     * @throws SQLException
     */
    public void readTable(String table) throws SQLException {
        Statement st = CONNECTION.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM " + table + "");

        while (rs.next()) {
            int id = rs.getInt("id");
            String nombre = rs.getString("nombre");
            int altura = rs.getInt("altura");
            int primeraAscension = rs.getInt("primeraAscension");
            String region = rs.getString("region");
            String pais = rs.getString("pais");

            List selectList = Arrays.asList(
                    "| ID: " + id,
                    "| NOMBRE: " + nombre,
                    "| ALTURA: " + altura,
                    "| PRIMERA ASCENSION: " + primeraAscension,
                    "| REGION: " + region,
                    "| PAIS: " + pais);
            selectList.forEach(out::println);
        }

        rs.close();
        if (st != null) st.close();

    }
}
