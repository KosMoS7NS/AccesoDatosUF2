package org.example.database.read;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;
import static org.example.database.Connect.CONNECTION;

/**
 * Consulta SQL en la cual devuelve una fila.
 * El usuario puede seleccionar la tabla, el campo y el valor a partir del cual desee realizar la búsqueda.
 * Mientras haya un ResultSet, va a ir almacenando en las variables respectivas para luego ser mostradas.
 */
public class ReadRow {
    /**
     *
     * @param table
     * @param field
     * @param value
     * @throws SQLException
     */
    public void readRow(String table, String field, String value) throws SQLException {
        Statement st = CONNECTION.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM " + table + " WHERE " + field + " = '" + value + "'");

        while (rs.next()) {
            int id = rs.getInt("id");
            String nombre = rs.getString("nombre");
            int altura = rs.getInt("altura");
            int primeraAscension = rs.getInt("primeraAscension");
            String region = rs.getString("region");
            String pais = rs.getString("pais");

            List selectList = Arrays.asList(
                    "| ID: " + id,
                    "| NOMBRE: " + nombre,
                    "| ALTURA: " + altura,
                    "| PRIMERA ASCENSION: " + primeraAscension,
                    "| REGION: " + region,
                    "| PAIS: " + pais);
            selectList.forEach(out::println);
        }

        rs.close();
        if (st != null) st.close();

    }
}
